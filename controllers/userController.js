const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports.registerUser = (req, res) => {

	let newUser = new User({

		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}


module.exports.loginUser = (req, res) => {
	const { email, password } = req.body;

	User.findOne({ email })
	.then(user => {
		if (!user) {

			return res.send({ message: "No user found" });

		}

		const isPasswordCorrect = bcrypt.compareSync(password, user.password);
		if (!isPasswordCorrect) {

			return res.send({ message: "Incorrect password" });
		}

		const accessToken = generateAccessToken(user);
		return res.send({ accessToken });
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	});
};

function generateAccessToken(user) {
	const payload = {
		id: user._id,
		email: user.email,
		mobileNo: user.mobileNo

	};

	const token = jwt.sign(payload, "your-secret-key",);

	return token;
}


module.exports.getUserEmail = (req, res) => {
	const { email } = req.body; 
	User.findOne({ email }) 
	.then(user => {
		if (user) {
			res.send(user);
		} else {
			res.send("User not found"); 
		}
	})
	.catch(error => {
		console.log(error);
		res.send("Error occurred");
	});
};